const PORT = 4000;

const express = require('express');
const os = require('os');

const app = express();
app.use(express.static(__dirname));
const server = require('http').createServer(app).listen(PORT);
const io = require('socket.io').listen(server);

// ログイン処理
const config = require("./config/discord");
const Discord = require('discord.js');
const client = new Discord.Client();
const token = config.token;

client.on('ready', () => {
    console.log('start server');
    console.log(`open chrome: http://localhost:${PORT}`);

    const targetTextChannel = client.channels.find(val => val.id === config.channel_id); // 指定テキストチャンネルを取得

    io.sockets.on('connection', socket => {
         socket.on('get_word', data => {
             if(data === undefined || data === "" || data === null) return;

             console.log(data);
             targetTextChannel.send(data.word);
        });
    });
});

client.login(token);