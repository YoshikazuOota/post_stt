## 使い方
### 初期設定
- windows に nodeをインストール
- コマンドプロンプトを開く
- npm install を実行(setting.bat)

### 使い方
- node server.js を実行(start.bat)
- chrome で http://localhost:4000/ を開く
- 表示されたページ内の開始ボタンを押す

# 使用ソースのライセンス表記
Bootstrap: Twitter, Inc. Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
jQuery v3.3.1: (c) JS Foundation and other contributors. jquery.org/license

## License
MIT License または Public Domain
* Public ドメインの場合でも、Qiita記事や著作者表記をしていただけると幸いです

## 利用アイコン
おむ烈 様
https://www.mildom.com/10039223