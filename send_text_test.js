// "ほげほげ"を書き込むテストコード
const config = require("./config/discord");

//ログイン処理
const Discord = require('discord.js');
const client = new Discord.Client();
const token = config.token;

const SEND_MESSAGE = 'ほげほげ';

client.on('ready', () => {
    const targetTextChannel = client.channels.find( value => value.id === config.channel_id);
    targetTextChannel.send(SEND_MESSAGE);
    console.log(`post "${SEND_MESSAGE}"`);
});
client.login(token);